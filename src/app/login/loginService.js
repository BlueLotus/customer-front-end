angular.module('customerFrontEnd')
    .factory('loginService', function($http, $cookies) {

        return {
            login: function(username, password) {
                var request = {
                    method: 'POST',
                    url: 'http://localhost:8080/rga-customer/login',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        username: username,
                        password: password
                    }
                };

                return $http(request).then(function(result) {
                    var credentials = result.data;
                    var token = 'Bearer ' + credentials.token;
                    $cookies.put('token', token);
                    $http.defaults.headers.common.Authorization = token;
                    return credentials;
                });
            }
        };
    });
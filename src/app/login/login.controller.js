angular.module('customerFrontEnd')
    .controller('LoginController', function($state, $log, loginService) {
        var vm = this;
        vm.submitForm = function() {
            if (vm.form.$valid) {
                loginService.login(vm.username, vm.password).then(function() {
                    $state.go('list');
                });
            }
        };
    });
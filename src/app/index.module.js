(function() {
  'use strict';

  angular
    .module('customerFrontEnd', ['ngAnimate','ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ui.router', 'ui.bootstrap', 'toastr']);

})();

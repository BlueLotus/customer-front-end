(function() {
  'use strict';

  angular
    .module('customerFrontEnd')
    .config(routerConfig)
    .run(function($http, $cookies) {
      var token = $cookies.get('token');
      $http.defaults.headers.common.Authorization = token;
    });

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'ctrl'
      })

      .state('login', {
        url: '/login',
        templateUrl: 'app/login/login.html',
        controller: 'LoginController',
        controllerAs: 'ctrl'
      })

      .state('list', {
        url: '/list',
        templateUrl: 'app/customers/list.html',
        controller: 'CustomerController',
        controllerAs: 'ctrl'
      });

    $urlRouterProvider.otherwise('/');

    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });
    $locationProvider.hashPrefix('!');
  }

})();

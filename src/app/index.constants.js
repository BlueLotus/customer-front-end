/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('customerFrontEnd')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();

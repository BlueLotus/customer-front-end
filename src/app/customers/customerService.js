angular.module('customerFrontEnd')
	.factory('customerService', function ($http) {

		return {
			getCustomers: function(params) {
				var request = {
					method: 'GET',
					url: 'http://localhost:8080/rga-customer/customer-service/customers',
					params: params,
					headers: {
						'Content-Type': 'application/json'
					}
				};

				return $http(request).then(function (result) {
					return result.data;
				});
			},

			getCustomer: function(customerId) {
				var request = {
					method: 'GET',
					url: 'http://localhost:8080/rga-customer/customer-service/customers/' + customerId,
					headers: {
						'Content-Type': 'application/json'
					}
				};
				return $http(request).then(function (result) {
					return result.data;
				});
			},

			postCustomer: function(customer) {
				var request = {
					method: 'POST',
					url: 'http://localhost:8080/rga-customer/customer-service/customer',
					headers: {
						'Content-Type': 'application/json'
					},
					data: customer
				};
				return $http(request).then(function (result) {
					return result.data;
				});
			},

			putCustomer: function(customer, customerId) {
				var request = {
					method: 'PUT',
					url: 'http://localhost:8080/rga-customer/customer-service/customers/' + customerId,
					headers: {
						'Content-Type': 'application/json'
					},
					data: customer
				};
				return $http(request).then(function (result) {
					return result.data;
				});
			}
		};

});
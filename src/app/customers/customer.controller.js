angular.module('customerFrontEnd')
    .controller('CustomerController', function($log, customerService) {
        var vm = this;

        vm.getCustomers = function() {
            customerService.getCustomers().then(function(response) {
                vm.customers = response;
                $log.log(response);
            });
        };

        vm.submitForm = function() {
            if (vm.form.$valid) {
                $log.log("in customer controller now.");
            }
        };

        vm.getCustomers();
    });